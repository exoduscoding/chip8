#include "stdio.h"
#include "stdlib.h"

#define ROM_START 0x200
#define RAM_SIZE 4096
#define NUM_REGISTERS 16
#define STACK_SIZE 16
#define SCREEN_WIDTH 64
#define SCREEN_HEIGHT 32


typedef struct {
    unsigned short opcode;
    unsigned char memory[RAM_SIZE];
    unsigned char registers[NUM_REGISTERS];
    unsigned short index_register;
    unsigned short program_counter;

    unsigned short stack[STACK_SIZE];
    unsigned short stack_pointer;

    unsigned char gfx[SCREEN_WIDTH * SCREEN_HEIGHT];

    unsigned char delay_timer;
    unsigned char sound_timer;

    unsigned char key[16];
} Chip8;



void init_chip8(Chip8 *chip8)
{
    for (int i = 0; i < RAM_SIZE; i++)
    {
        chip8->memory[i] = 0;
    }

    for (int i = 0; i < STACK_SIZE; i++)
    {
        chip8->stack[i] = 0;
    }

    for (int i = 0; i < NUM_REGISTERS; i++)
    {
        chip8->registers[i] = 0;
    }

    for (int i = 0; i < (SCREEN_WIDTH*SCREEN_HEIGHT); i++)
    {
        chip8->gfx[i] = 0;
    }

    chip8->program_counter = ROM_START;
    chip8->opcode = 0;
    chip8->index_register = 0;
    chip8->stack_pointer = 0;
}


int read_rom(Chip8 *chip8, char *rom_path)
{
    FILE *file;
    int file_size;

    file = fopen(rom_path, "rb");
    if (file == NULL)
    {
        printf("ruh roh\n");
        return -1;
    } else {
        fseek(file, 0, SEEK_END);
        file_size = ftell(file);
        // printf("file size: %d\n", file_size);
        rewind(file);

        fread(chip8->memory + ROM_START, 1, file_size, file);
    }
    fclose(file);

    return 0;
}

void print_rom(Chip8 *chip8)
{
    int count = 0;
    printf("program:\n");

    for (int i = ROM_START; i < RAM_SIZE; i++)
    {
        if (chip8->memory[i]) {
            printf("%02X ", chip8->memory[i]);
            count++;
        }
        if (count == 16)
        {
            printf("\n");
            count = 0;
        }
    }
    printf("\n");
}

int main(int argc, char **argv, char **envp)
{
    if (argc <= 1)
    {
        printf("Usage: %s <rom>\n", argv[0]);
        return -1;
    }

    Chip8 *chip8 = malloc(sizeof(Chip8));
    init_chip8(chip8);

    read_rom(chip8, argv[1]);
    print_rom(chip8);


    return 0;
}
